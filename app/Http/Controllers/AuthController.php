<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name'=>'required', 
            'email'=>'required|email|unique:users',
            'password'=>'required'
        ]);


        if($validator->fails()) {
                $response = [
                'success'=> true, 
                'message'=> $validator->errors()
            ];

            return response()->json($response, 403);
        }

        $input['password']=bcrypt($input['password']); 

        $user = User::create($input);

         $response = [
            'token'=> $user->createToken('secretcoding')->plainTextToken, 
            'name'=> $user->name, 
            'email'=>$user->email, 
            'message'=>'User successfully Registered'
        ];

        return response()->json($response, 200);
    }

    public function login(Request $request)
    {
    	 $input = $request->all();

        $validator = Validator::make($input, [
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if($validator->fails()) {
                $response = [
                'success'=> true, 
                'message'=> $validator->errors()
            ];

            return response()->json($response, 403);
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {


        	$user = Auth::user();

        	$response = [
            'token'=> $user->createToken('secretcoding')->plainTextToken, 
            'name'=> $user->name, 
            'email'=>$user->email, 
            'message'=>'User successfully Login'
        ];

        return response()->json($response, 200);
		} else {
			return response()->json('Your email or password is not valid', 200);
		}
	}
}
